# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests"""
from contextlib import contextmanager
import os.path
import re
import subprocess
import sys
import tempfile
import textwrap
import unittest

# Initial command-line arguments invoking kpet
KPET_ARGV = []

# If running under "coverage"
if "coverage" in sys.modules:
    # Run our invocations of kpet under "coverage" as well to collect coverage
    # NOTE Keep command line in sync with setup.cfg
    KPET_ARGV += "coverage run -p --branch --source=kpet".split(" ")

# Add path to in-tree kpet executable, relative for more readable output
KPET_ARGV += [os.path.relpath(os.path.join(os.path.dirname(__file__),
                                           "../bin/kpet"))]

BEAKER_XML_J2 = """
<job>
  {% for scene in SCENES %}
    {% for recipeset in scene.recipesets %}
      {% for HOST in recipeset %}
        HOST
        {% for test in HOST.tests %}
          {{ test.name }}
        {% endfor %}
        {% if HOST.preboot_tasks %}
          {% include HOST.preboot_tasks %}
        {% endif %}
        {% if HOST.postboot_tasks %}
          {% include HOST.postboot_tasks %}
        {% endif %}
      {% endfor %}
    {% endfor %}
  {% endfor %}
</job>
"""

INDEX_BASE_YAML = """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                case:
                  host_type_regex: ^normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
"""


def get_patch_path(patch_name):
    """
    Return the patch asset path for a patch name.

    Args:
        patch_name: Patch name (a subdir in the patch asset directory).

    Returns:
        The full path to the patch asset directory.
    """
    return os.path.relpath(os.path.join(os.path.dirname(__file__),
                                        "assets/patches", patch_name))


def kpet(*args):
    """
    Execute kpet with specified arguments.

    Args:
        args:   Command-line arguments to pass to kpet.

    Returns:
        Exit status, standard output, standard error
    """
    process = subprocess.run(KPET_ARGV + list(args),
                             check=False,
                             capture_output=True,
                             encoding='utf8')
    return (process.returncode, process.stdout, process.stderr)


def kpet_with_db(db_path, *args):
    """
    Execute kpet with a database directory specified, and optional extra
    arguments.

    Args:
        db_path:  Path to the database directory.
        args:     Extra command-line arguments to pass to kpet.

    Returns:
        Exit status, standard output, standard error
    """
    return kpet("--db", db_path, *args)


def kpet_run(db_path, command_name_args, *args):
    """
    Execute a "kpet run" command with a database directory specified, tree
    "tree", architecture "arch", and optional extra arguments.

    Args:
        db_path:            Path to the database directory.
        command_name_args:  Arguments specifying the "run" subcommand.
        args:               Extra command-line arguments to pass to kpet.

    Returns:
        Exit status, standard output, standard error
    """
    return kpet_with_db(db_path, "--debug", "run", *command_name_args,
                        "-t", "tree", "-a", "arch", *args)


def kpet_run_generate(db_path, *args):
    """
    Execute "kpet run generate" with a database directory specified, tree
    "tree", architecture "arch", kernel "kernel.tar.gz", and optional
    extra arguments.

    Args:
        db_path:    Path to the database directory.
        args:       Extra command-line arguments to pass to kpet.

    Returns:
        Exit status, standard output, standard error
    """
    return kpet_run(db_path, ["generate"], "-k", "kernel.tar.gz", *args)


def kpet_run_test_list(db_path, *args):
    """
    Execute "kpet run test list" with a database directory specified, tree
    "tree", architecture "arch", and optional extra arguments.

    Args:
        db_path:    Path to the database directory.
        args:       Extra command-line arguments to pass to kpet.

    Returns:
        Exit status, standard output, standard error
    """
    return kpet_run(db_path, ["test", "list"], *args)


def kpet_test_list(db_path, *args):
    """
    Execute "kpet test list" with a database directory specified, and optional
    extra arguments.

    Args:
        db_path:    Path to the database directory.
        args:       Extra command-line arguments to pass to kpet.

    Returns:
        Exit status, standard output, standard error
    """
    return kpet_with_db(db_path, "--debug", "test", "list", *args)


@contextmanager
def assets_mkdir(assets):
    """
    Context manager creating (and removing) a temporary directory containing
    test asset files specified as a dictionary of names of files and their
    contents.

    Args:
        assets: A dictionary where the keys are the filenames and values
                the contents of the files

    Returns:
        Created directory path.
    """
    assert isinstance(assets, dict)
    assert all(isinstance(name, str) and isinstance(contents, str)
               for name, contents in assets.items())
    with tempfile.TemporaryDirectory(prefix="", suffix=".kpet-test-assets") \
            as dir_path:
        for filename, contents in assets.items():
            with open(os.path.join(dir_path, filename), 'w') as asset_file:
                asset_file.write(contents)
        yield dir_path


class IntegrationTests(unittest.TestCase):
    """Integration tests"""

    # pylint: disable=invalid-name,no-self-use
    # (matching unittest conventions)
    def assertKpetProduces(self, func, *args,
                           status=0,
                           stdout_matching="",
                           stderr_matching=""):
        """
        Assert execution of a kpet-running function produces a particular exit
        status, and stderr/stdout output.

        Args:
            func:               Function executing kpet. Must return kpet's
                                exit status, stdout, and stderr.
            args:               Arguments to pass to "func".
            status:             Exit status kpet should produce.
                                Zero, if not specified.
            stdout_matching:    String representation of a regular expression,
                                which stdout should match fully.
                                "" if not specified.
            stderr_matching:    String representation of a regular expression,
                                which stderr should match fully.
                                "" if not specified.
        """
        errors = []
        result_status, result_stdout, result_stderr = func(*args)
        if result_status != status:
            errors.append("Expected exit status {}, got {}".
                          format(status, result_status))
        if not re.fullmatch(stdout_matching, result_stdout, re.DOTALL):
            errors.append("Stdout doesn't match regex \"{}\":\n{}".
                          format(stdout_matching,
                                 textwrap.indent(result_stdout, "    ")))
        if not re.fullmatch(stderr_matching, result_stderr, re.DOTALL):
            errors.append("Stderr doesn't match regex \"{}\":\n{}".
                          format(stderr_matching,
                                 textwrap.indent(result_stderr, "    ")))
        if errors:
            raise AssertionError("\n".join(errors))

    def assertKpetSrcMatchesTwoCases(self, db_path):
        """
        Assert kpet source-matches two cases properly.

        Args:
            db_path:    Path to the directory containing the database to test
                        with.
        """
        # Both appear in baseline output
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                            r'case2\s*</job>.*')
        # One appears with its patches
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            get_patch_path("misc/files_abc.diff"),
            stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
        # Another appears with its patches
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            get_patch_path("misc/files_def.diff"),
            stdout_matching=r'.*<job>\s*HOST\s*case2\s*</job>.*')
        # Both appear with their patches
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            get_patch_path("misc/files_abc.diff"),
            get_patch_path("misc/files_def.diff"),
            stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                            r'case2\s*</job>.*')
        # None appear with other patches
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            get_patch_path("misc/files_ghi.diff"),
            stdout_matching=r'.*<job>\s*</job>.*')

    def assertKpetSrcMatchesOneOfTwoCases(self, db_path):
        """
        Assert kpet source-matches one of two cases properly.

        Args:
            db_path:    Path to the directory containing the database to test
                        with.
        """
        # Only one appears in baseline output
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
        # One appears with its patches
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            get_patch_path("misc/files_abc.diff"),
            stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
        # Another doesn't appear with its patches
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            get_patch_path("misc/files_def.diff"),
            stdout_matching=r'.*<job>\s*</job>.*')
        # Only one appears with both case's patches
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            get_patch_path("misc/files_abc.diff"),
            get_patch_path("misc/files_def.diff"),
            stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
        # None appear with other patches
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            get_patch_path("misc/files_ghi.diff"),
            stdout_matching=r'.*<job>\s*</job>.*')

    def assertKpetSrcMatchesNoneOfTwoCases(self, db_path):
        """
        Assert kpet source-matches none of two cases properly.

        Args:
            db_path:    Path to the directory containing the database to test
                        with.
        """
        # They don't appear in baseline output
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            stdout_matching=r'.*<job>\s*</job>.*')
        # They don't appear when all of their patches and extras are specified
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            get_patch_path("misc/files_abc.diff"),
            get_patch_path("misc/files_def.diff"),
            get_patch_path("misc/files_ghi.diff"),
            stdout_matching=r'.*<job>\s*</job>.*')

    def assertKpetSchemaInvalidError(self, db_path, expectedError):
        """
        Assert kpet raises a Schema Invalid error.

        Args:
            db_path:    Path to the directory containing the database to test
                        with.
        """
        # It is thrown in the baseline output
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            status=1,
            stdout_matching=r'.*',
            stderr_matching=r'.*kpet.schema.Invalid: ' + expectedError + '.*')
        # It is still thrown when all of the patches and extras are specified
        self.assertKpetProduces(
            kpet_run_generate, db_path,
            get_patch_path("misc/files_abc.diff"),
            get_patch_path("misc/files_def.diff"),
            get_patch_path("misc/files_ghi.diff"),
            status=1,
            stdout_matching=r'.*',
            stderr_matching=r'.*kpet.schema.Invalid: ' + expectedError + '.*')
