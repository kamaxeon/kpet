# Copyright (c) 2020 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for test name handling"""
from tests.test_integration import (IntegrationTests, kpet_run_generate,
                                    assets_mkdir)

DATABASE_YAML = """
            host_types:
                normal: {}
            recipesets:
                rcs1:
                  - normal
            arches:
                - arch
            trees:
                tree: {}
            template: output.txt.j2
            case:
                host_type_regex: ^normal
                maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                location: somewhere
                max_duration_seconds: 100
                cases:
"""


class IntegrationNamesTests(IntegrationTests):
    """Integration tests for test name handling"""

    def test_suite_names(self):
        """Test repeated and non-repeated test names work correctly"""

        # Single empty test name
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            cases:
                                case1: {}
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'^$')

        # Double empty test name from leaves
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            cases:
                              case1: {}
                              case2: {}
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=1,
                stderr_matching=r".* non-unique name: \n")

        # Double empty test name from non-leaves
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            cases:
                                case1: {}
                        case1:
                            cases:
                                case1: {}
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'^$')

        # Double non-empty test name from leaves
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            cases:
                                case1:
                                  name: case
                                case2:
                                  name: case
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=1,
                stderr_matching=r".* non-unique name: case\n")

        # Double non-empty test name from non-leaves
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            name: suite
                            cases:
                                case1: {}
                                case2: {}
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=1,
                stderr_matching=r".* non-unique name: suite\n")

        # Double non-empty test name from two leaf cases
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            cases:
                                case1:
                                    name: case
                        case2:
                            cases:
                                case1:
                                    name: case
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=1,
                stderr_matching=r".* non-unique name: case\n")

        # Double non-empty test name from two non-leaf cases
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            name: case
                            cases: {}
                        case2:
                            name: case
                            cases: {}
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'^$')

        # Double assymetric-placement test name
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            name: foo
                            cases: {}
                        case2:
                            cases:
                                case1:
                                    name: foo
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'^$')

        # Single empty test name
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            cases: {}
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'^$')

        # Single non-empty case name
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            cases:
                                case1:
                                    name: case
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'^$')

        # Single non-empty non-leaf and leaf name
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            name: case
                            cases:
                                case1:
                                    name: case
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'^$')

        # Unique leaf case names
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            cases:
                                case1:
                                    name: case1
                                case2:
                                    name: case2
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'^$')

        # Unique non-leaf names
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        case1:
                            name: case1
                            cases: {}
                        case2:
                            name: case2
                            cases: {}
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'^$')

        # Unique non-leaf and leaf names
        with assets_mkdir({
                    "index.yaml": DATABASE_YAML + """
                        CASE1:
                            name: CASE1
                            cases:
                              case1:
                                name: case1
                        CASE2:
                            name: CASE2
                            cases:
                              case2:
                                name: case2
                    """,
                    "output.txt.j2": ""
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'^$')
