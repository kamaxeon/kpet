# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests expecting a match"""
from tests.test_integration import (IntegrationTests, kpet_run_generate,
                                    BEAKER_XML_J2, assets_mkdir)


class IntegrationMatchTreesArchesTests(IntegrationTests):
    """Integration tests expecting a match in arches or trees"""

    def test_match_arches_no_patterns(self):
        """Test architecture-matching a case with no patterns"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - ""
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                case:
                  host_type_regex: ^normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      enabled:
                        arches:
                          or: []
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Doesn't match a non-empty (default "arch") architecture
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*</job>.*')
            # Doesn't match empty architecture
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-a", "",
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_match_arches_one_pattern(self):
        """Test architecture-matching a case with one pattern"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - ""
                    - arch
                    - not_arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                case:
                  host_type_regex: ^normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      enabled:
                          arches: arch
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches default ("arch") architecture
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Doesn't match empty architecture
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-a", "",
                stdout_matching=r'.*<job>\s*</job>.*')
            # Doesn't match another architecture
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-a", "not_arch",
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_match_arches_two_patterns(self):
        """Test architecture-matching a case with two patterns"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - ""
                    - arch
                    - not_arch
                    - other_arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                case:
                  host_type_regex: ^normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      enabled:
                        arches:
                          or:
                            - arch
                            - other_arch
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches default ("arch") architecture
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches non-default (but listed) architecture
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "-a", "other_arch",
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Doesn't match empty architecture
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-a", "",
                stdout_matching=r'.*<job>\s*</job>.*')
            # Doesn't match another architecture
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-a", "not_arch",
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_match_trees_no_patterns(self):
        """Test tree-matching a case with no patterns"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    "": {}
                    tree: {}
                template: beaker.xml.j2
                case:
                  host_type_regex: ^normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      enabled:
                        trees:
                          or: []
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Doesn't match a non-empty (default "tree") tree
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*</job>.*')
            # Doesn't match empty tree
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-t", "",
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_match_trees_one_pattern(self):
        """Test tree-matching a case with one pattern"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    "": {}
                    tree: {}
                    not_tree: {}
                template: beaker.xml.j2
                case:
                  host_type_regex: ^normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      enabled:
                          trees: tree
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches default ("tree") tree
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Doesn't match empty tree
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-t", "",
                stdout_matching=r'.*<job>\s*</job>.*')
            # Doesn't match another tree
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-t", "not_tree",
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_match_trees_two_patterns(self):
        """Test tree-matching a case with two patterns"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    "": {}
                    tree: {}
                    not_tree: {}
                    other_tree: {}
                template: beaker.xml.j2
                case:
                  host_type_regex: ^normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      enabled:
                        trees:
                          or:
                            - tree
                            - other_tree
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches default ("tree") tree
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches non-default (but listed) tree
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "-t", "other_tree",
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Doesn't match empty tree
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-t", "",
                stdout_matching=r'.*<job>\s*</job>.*')
            # Doesn't match another tree
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-t", "not_tree",
                stdout_matching=r'.*<job>\s*</job>.*')
