# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Template variable integration tests"""
from tests.test_integration import (IntegrationTests, kpet_run_generate,
                                    assets_mkdir)


# index.yaml with a required template variable "x"
INDEX_YAML_REQUIRED_VARIABLE_X = """
    host_types:
        normal: {}
    recipesets:
        rcs1:
          - normal
    arches:
        - arch
    trees:
        tree: {}
    template: beaker.xml.j2
    variables:
        x:
            description: Variable x
"""

# index.yaml with an optional template variable "x"
INDEX_YAML_OPTIONAL_VARIABLE_X = \
    INDEX_YAML_REQUIRED_VARIABLE_X + """
            default: DEFAULT
"""

# Database outputting a required template variable "x"
DB_REQUIRED_VARIABLE_X = {
    "index.yaml": INDEX_YAML_REQUIRED_VARIABLE_X,
    "beaker.xml.j2": "{{ VARIABLES.x }}"
}

# Database outputting an optional template variable "x"
DB_OPTIONAL_VARIABLE_X = {
    "index.yaml": INDEX_YAML_OPTIONAL_VARIABLE_X,
    "beaker.xml.j2": "{{ VARIABLES.x }}"
}


class IntegrationVariablesTests(IntegrationTests):
    """Integration tests for template variable interface"""

    def test_short_opt(self):
        """Check short option is accepted"""
        with assets_mkdir(DB_REQUIRED_VARIABLE_X) as db_path:
            self.assertKpetProduces(kpet_run_generate, db_path,
                                    "-v", "x=VALUE", "--no-lint",
                                    stdout_matching=r'VALUE')

    def test_long_opt(self):
        """Check long option is accepted"""
        with assets_mkdir(DB_REQUIRED_VARIABLE_X) as db_path:
            self.assertKpetProduces(kpet_run_generate, db_path,
                                    "--variable", "x=VALUE", "--no-lint",
                                    stdout_matching=r'VALUE')

    def test_required_not_specified(self):
        """Check not specifying a required variable aborts rendering"""
        with assets_mkdir(DB_REQUIRED_VARIABLE_X) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=1,
                stderr_matching=r'.*Required variables not set: x\..*')

    def test_unknown_specified(self):
        """Check specifying an unknown variable aborts rendering"""
        with assets_mkdir(DB_REQUIRED_VARIABLE_X) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint", "-v", "y=VALUE", status=1,
                stderr_matching=r'.*Unknown variables specified: y\..*')

    def test_optional_not_specified(self):
        """Check not specifying an optional variable outputs defaults"""
        with assets_mkdir(DB_OPTIONAL_VARIABLE_X) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'DEFAULT')

    def test_optional_specified(self):
        """Check specifying an optional variable overrides default"""
        with assets_mkdir(DB_OPTIONAL_VARIABLE_X) as db_path:
            self.assertKpetProduces(kpet_run_generate, db_path,
                                    "-v", "x=VALUE", "--no-lint",
                                    stdout_matching=r'VALUE')
