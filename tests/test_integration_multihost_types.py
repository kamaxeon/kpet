# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration multihost tests"""
from tests.test_integration import (IntegrationTests, kpet_run_generate,
                                    BEAKER_XML_J2, assets_mkdir)


class IntegrationMultihostTypesTests(IntegrationTests):
    """Multihost integration tests with at least one type"""

    def test_multihost_one_type_no_regex_no_suites(self):
        """Test multihost support with one type, but no regex/suites"""
        assets = {
            "index.yaml": """
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                host_types:
                    normal: {}
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetSrcMatchesNoneOfTwoCases(db_path)

    def test_multihost_one_type_no_regex_two_suites(self):
        """Test multihost support with one type, no regex, and two cases"""
        assets = {
            "index.yaml": """
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                host_types:
                    normal: {}
                case:
                  max_duration_seconds: 600
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    case1:
                      name: case1
                      enabled:
                        sources:
                          or:
                            - a
                    case2:
                      name: case2
                      enabled:
                        sources:
                          or:
                            - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetSrcMatchesTwoCases(db_path)

    def test_multihost_one_type_db_regex(self):
        """Test multihost support with one type and root-level regex"""
        assets = {
            "index.yaml": """
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                host_types:
                    normal: {}
                case:
                  host_type_regex: normal
                  max_duration_seconds: 600
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    case1:
                      name: case1
                      enabled:
                        sources:
                          or:
                            - a
                    case2:
                      name: case2
                      enabled:
                        sources:
                          or:
                            - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetSrcMatchesTwoCases(db_path)

    def test_multihost_one_type_non_leaf_regex(self):
        """
        Test multihost support with one-type recipeset and non-leaf regexes
        """
        assets = {
            "index.yaml": """
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                host_types:
                    normal: {}
                    not_normal: {}
                case:
                  cases:
                    CASE1:
                      location: somewhere
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      host_type_regex: normal
                      cases:
                        case1:
                          name: case1
                          max_duration_seconds: 600
                    CASE2:
                      location: somewhere
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      host_type_regex: not_normal
                      cases:
                        case2:
                          name: case2
                          max_duration_seconds: 600
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                                r'HOST\s*case2\s*</job>.*')

    def test_multihost_one_type_leaf_regexes(self):
        """
        Test multihost support with with one-type recipeset and leaf regexes
        """
        assets = {
            "index.yaml": """
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                host_types:
                    normal: {}
                    not_normal: {}
                case:
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  max_duration_seconds: 600
                  cases:
                    case1:
                      name: case1
                      host_type_regex: normal
                    case2:
                      name: case2
                      host_type_regex: not_normal
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                                r'\s*HOST\s*case2\s*</job>.*')

    def test_multihost_two_types_one_case_each(self):
        """Test multihost support with two types matching one case each"""
        assets = {
            "index.yaml": """
                recipesets:
                    rcs1:
                      - a
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                host_types:
                    a: {}
                    b: {}
                case:
                  host_type_regex: .*
                  max_duration_seconds: 600
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    case1:
                      name: case1
                      host_type_regex: a
                      enabled:
                        sources:
                          or:
                            - a
                    case2:
                      name: case2
                      host_type_regex: b
                      enabled:
                        sources:
                          or:
                            - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                                r'HOST\s*case2\s*</job>.*')

    def test_multihost_two_types_both_cases_first(self):
        """
        Test multihost support with two types and both cases matching the
        first one.
        """
        assets = {
            "index.yaml": """
                recipesets:
                    rcs1:
                      - a
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                host_types:
                    a: {}
                    b: {}
                case:
                  host_type_regex: .*
                  max_duration_seconds: 600
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    case1:
                      name: case1
                      host_type_regex: a
                      enabled:
                        sources:
                          or:
                            - a
                    case2:
                      name: case2
                      host_type_regex: a
                      enabled:
                        sources:
                          or:
                            - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # TODO Distinguish host types somehow
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*case2\s*</job>.*')

    def test_multihost_two_types_both_cases_second(self):
        """
        Test multihost support with two types and both cases matching the
        second one.
        """
        assets = {
            "index.yaml": """
                recipesets:
                    rcs1:
                      - a
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                host_types:
                    a: {}
                    b: {}
                case:
                  host_type_regex: .*
                  max_duration_seconds: 600
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    case1:
                      name: case1
                      host_type_regex: b
                      enabled:
                        sources:
                          or:
                            - a
                    case2:
                      name: case2
                      host_type_regex: b
                      enabled:
                        sources:
                          or:
                            - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # TODO Distinguish host types somehow
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*case2\s*</job>.*')

    def test_multihost_two_types_both_cases_both(self):
        """
        Test multihost support with two types and both cases matching both
        types.
        """
        assets = {
            "index.yaml": """
                recipesets:
                    rcs1:
                      - a
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                host_types:
                    a: {}
                    b: {}
                case:
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  max_duration_seconds: 600
                  cases:
                    case1:
                      name: case1
                      host_type_regex: ".*"
                      enabled:
                        sources:
                          or:
                            - a
                    case2:
                      name: case2
                      host_type_regex: ".*"
                      enabled:
                        sources:
                          or:
                            - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # TODO Distinguish host types somehow
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*case2\s*</job>.*')

    def test_multihost_one_type_suite_wrong_regex(self):
        """Test multihost schema invalid error with wrong non-leaf regexes"""
        assets = {
            "index.yaml": """
            recipesets:
                rcs1:
                  - normal
            arches:
                - arch
            trees:
                tree: {}
            template: beaker.xml.j2
            host_types:
                normal: {}
            case:
              maintainers:
                - name: maint1
                  email: maint1@maintainers.org
              host_type_regex: ^normal
              max_duration_seconds: 600
              cases:
                CASE1:
                  location: somewhere
                  host_type_regex: normal
                  cases:
                    case1:
                      name: case1
                      enabled:
                        sources:
                          or:
                            - a
                CASE2:
                  location: somewhere
                  host_type_regex: not_normal
                  cases:
                    case2:
                      name: case2
                      enabled:
                        sources:
                          or:
                            - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetSchemaInvalidError(
                db_path,
                "Host type regex \"not_normal\" .* does not match")

    def test_multihost_one_type_case_wrong_regex(self):
        """Test multihost schema invalid error with wrong leaf regexes"""
        assets = {
            "index.yaml": """
            recipesets:
                rcs1:
                  - normal
            arches:
                - arch
            trees:
                tree: {}
            template: beaker.xml.j2
            host_types:
                normal: {}
            case:
              host_type_regex: ^normal
              max_duration_seconds: 600
              maintainers:
                - name: maint1
                  email: maint1@maintainers.org
              cases:
                case1:
                  name: case1
                  host_type_regex: normal
                  enabled:
                    sources:
                      or:
                        - a
                case2:
                  name: case2
                  host_type_regex: not_normal
                  enabled:
                    sources:
                      or:
                        - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetSchemaInvalidError(
                db_path,
                "Host type regex \"not_normal\" .* does not match")

    def test_multihost_one_type_wrong_regex(self):
        """Test multihost schema invalid error with wrong root regex"""

        assets = {
            "index.yaml": """
            recipesets:
                rcs1:
                  - normal
            arches:
                - arch
            trees:
                tree: {}
            template: beaker.xml.j2
            host_types:
                normal: {}
            case:
              host_type_regex: not_normal
              max_duration_seconds: 600
              maintainers:
                - name: maint1
                  email: maint1@maintainers.org
              cases:
                case1:
                  name: case1
                  enabled:
                    sources:
                      or:
                        - a
                case2:
                  name: case2
                  enabled:
                    sources:
                      or:
                        - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetSchemaInvalidError(
                db_path,
                "Host type regex \"not_normal\" .* does not match")
