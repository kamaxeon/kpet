# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for source file matching"""
from tests.test_integration import (IntegrationTests, kpet_run_generate,
                                    get_patch_path, BEAKER_XML_J2,
                                    assets_mkdir, INDEX_BASE_YAML)


class IntegrationMatchSourcesTests(IntegrationTests):
    """Integration tests for source file matching"""

    def test_match_sources_one_case_no_patterns(self):
        """Test source-matching a case with no patterns"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')

    def test_match_sources_one_case_one_pattern(self):
        """Test source-matching a case with one pattern"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      enabled:
                        sources:
                          or: a
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches patches it should
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Doesn't match patches it shouldn't
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_match_sources_one_case_two_patterns(self):
        """Test source-matching a case with two patterns"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      enabled:
                        sources:
                          or:
                            - a
                            - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches first patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches second patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Matches both patches only once
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Doesn't match patches it shouldn't
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_match_sources_two_cases(self):
        """Test source-matching two cases"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      enabled:
                        sources:
                          or: a
                    case2:
                      name: case2
                      max_duration_seconds: 600
                      enabled:
                        sources:
                          or: d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Both match baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                                r'case2\s*</job>.*')
            # First matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Second matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case2\s*</job>.*')
            # Both match their patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                                r'case2\s*</job>.*')
            # None match other patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_match_sources_two_suites(self):
        """Test source-matching two suites"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      enabled:
                        sources:
                          or: a
                    case2:
                      name: case2
                      max_duration_seconds: 600
                      enabled:
                        sources:
                          or: d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Both match baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                                r'case2\s*</job>.*')
            # First matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*</job>.*')
            # Second matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case2\s*</job>.*')
            # Both match their patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*case1\s*'
                                r'case2\s*</job>.*')
            # None match other patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_match_sources_specific_suite(self):
        """Test source-matching with a specific abstract case"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      enabled:
                        not:
                          sources: null
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                          enabled:
                            sources: a
                    B:
                      name: B
                      cases:
                        b:
                          name: b
                          max_duration_seconds: 600
                          enabled:
                            sources: d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Only non-specific suite matches baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*B - b\s*</job>.*')
            # First matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')
            # Second matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*B - b\s*</job>.*')
            # All suites can match if provided appropriate patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*'
                                r'B - b\s*</job>.*')
            # None match other patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')

    def test_match_sources_specific_case(self):
        """Test source-matching with a specific test case"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                          enabled:
                            not:
                              sources: null
                            sources: a
                    B:
                      name: B
                      cases:
                        b:
                          name: b
                          max_duration_seconds: 600
                          enabled:
                            sources: d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Only non-specific case matches baseline
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*B - b\s*</job>.*')
            # First matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')
            # Second matches its patch
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*B - b\s*</job>.*')
            # All cases can match if provided appropriate patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_abc.diff"),
                get_patch_path("misc/files_def.diff"),
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*'
                                r'B - b\s*</job>.*')
            # None match other patches
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                get_patch_path("misc/files_ghi.diff"),
                stdout_matching=r'.*<job>\s*</job>.*')
