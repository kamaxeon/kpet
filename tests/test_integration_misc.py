# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration miscellaneous tests"""
from tests.test_integration import (IntegrationTests, kpet_run_generate,
                                    kpet_run_test_list,
                                    kpet_with_db, BEAKER_XML_J2,
                                    INDEX_BASE_YAML, assets_mkdir)


class IntegrationMiscTests(IntegrationTests):
    # Calm down, pylint: disable=too-many-public-methods
    """Miscellaneous integration tests"""
    # It's OK pylint: disable=too-many-public-methods

    def test_empty_tree_list(self):
        """Test tree listing with empty database"""
        assets = {
            "index.yaml": """
                # Empty but valid database
                {}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_with_db, db_path,
                                    "-d", "tree", "list")

    def test_empty_run_generate(self):
        """Test run generation with empty database"""
        assets = {
            "index.yaml": """
                # Empty but valid database
                {}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=1,
                stderr_matching=r'.*Tree "tree" not found.*')

    def test_empty_run_test_list(self):
        """Test run test listing with empty database"""
        assets = {
            "index.yaml": """
                # Empty but valid database
                {}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                status=1,
                stderr_matching=r'.*Tree "tree" not found.*')

    def test_minimal_run_generate(self):
        """Test run generation with minimal database"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                    panicky:
                        ignore_panic: true
                    multihost_1: {}
                    multihost_2: {}
                recipesets:
                    rcs1:
                      - normal
                      - panicky
                    rcs2:
                      - multihost_1
                      - multihost_2

                # Minimal, but fully-functional database
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_run_generate, db_path,
                                    stdout_matching=r'.*<job>\s*</job>.*')

    def test_minimal_run_test_list(self):
        """Test run test listing with minimal database"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                    panicky:
                        ignore_panic: true
                    multihost_1: {}
                    multihost_2: {}
                recipesets:
                    rcs1:
                      - normal
                      - panicky
                    rcs2:
                      - multihost_1
                      - multihost_2

                # Minimal, but fully-functional database
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_run_test_list, db_path)

    def test_missing_template_run_generate(self):
        """Test run generation with a missing template"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                    panicky:
                        ignore_panic: true
                    multihost_1: {}
                    multihost_2: {}
                recipesets:
                    rcs1:
                      - normal
                      - panicky
                    rcs2:
                      - multihost_1
                      - multihost_2

                arches:
                    - arch
                trees:
                    tree: {}
                template: missing_template.xml.j2
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_run_generate,
                                    db_path,
                                    "-t", "tree",
                                    status=1,
                                    stderr_matching=r'.*TemplateNotFound.*')

    def test_missing_case_file_run_generate(self):
        """Test run generation with a missing case file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                trees:
                    tree: {}
                template: beaker.xml.j2
                case: missing.yaml
            """,
            "beaker.xml.j2": """
                <job/>
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_run_generate,
                                    db_path,
                                    status=1,
                                    stderr_matching=r'.*missing.yaml.*')

    def test_invalid_top_yaml_tree_list(self):
        """Test tree listing with invalid YAML in the top database file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                tree: {
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_with_db, db_path,
                                    "--debug", "tree", "list",
                                    status=1,
                                    stderr_matching=r'.*ParserError.*')

    def test_invalid_case_yaml_tree_list(self):
        """Test tree listing with invalid YAML in a case file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                case: case.yaml
            """,
            "case.yaml": """
                {
                maintainers:
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_with_db, db_path,
                                    "--debug", "tree", "list",
                                    status=1,
                                    stderr_matching=r'.*ParserError.*')

    def test_invalid_top_data_tree_list(self):
        """Test tree listing with invalid data in the top database file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                trees: {}
                unknown_node: True
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_with_db, db_path,
                                    "tree", "list",
                                    status=1,
                                    stderr_matching=r'.*Invalid database.*')

    def test_invalid_case_data_tree_list(self):
        """Test tree listing with invalid data in a case file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                case: case.yaml
            """,
            "case.yaml": """
                name: "Case data with unknown nodes"
                foobar: True
                location: somewhere
                maintainers:
                  - name: maint1
                    email: maint1@maintainers.org
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_with_db, db_path,
                                    "tree", "list",
                                    status=1,
                                    stderr_matching=r'.*Invalid test case.*')

    def test_empty_abstract_case_run_generate(self):
        """Test run generation with an empty abstract case"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                case:
                  host_type_regex: ^normal
                  cases: {}
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_run_generate, db_path,
                                    stdout_matching=r'.*<job>\s*</job>.*')

    def test_empty_case_no_patterns_run_generate(self):
        """Test run generation with an empty test case without patterns"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')

    def test_empty_case_with_a_pattern_run_generate(self):
        """Test run generation with an empty test case with a pattern"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')

    def test_preboot_tasks_are_added(self):
        """Test preboot tasks are added"""
        assets = {
            "index.yaml": """
                host_types:
                    normal:
                        preboot_tasks: preboot_tasks.xml.j2
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    max_duration_seconds: 600
                    host_type_regex: ^normal
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
            "preboot_tasks.xml.j2": "<task>Some preboot task</task>"
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<task>\s*Some preboot task\s*</task>.*')

    def test_cases_expose_their_name(self):
        """Test case's "name" field should be exposed to Beaker templates"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: case A
                      location: somewhere
                      cases:
                        a:
                          name: case a
                          max_duration_seconds: 600
                          maintainers:
                            - name: maint1
                              email: maint1@maintainers.org
            """,
            "beaker.xml.j2": """
            <job>
              {% for scene in SCENES %}
                {% for recipeset in scene.recipesets %}
                  {% for HOST in recipeset %}
                    {% for test in HOST.tests %}
                      {{ test.name }}
                    {% endfor %}
                  {% endfor %}
                {% endfor %}
              {% endfor %}
            </job>
            """,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*case A - case a\s*</job>.*')

    def test_cases_expose_their_maintainers(self):
        """Test cases' "maintainers" field should be exposed to templates"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      maintainers:
                        - name: Some Maintainer
                          email: someone@maintainers.org
            """,
            "beaker.xml.j2": """
            <job>
              {% for scene in SCENES %}
                {% for recipeset in scene.recipesets %}
                  {% for HOST in recipeset %}
                    {% for test in HOST.tests %}
                      {{ test.name }}
                      Maintainers:
                      {% for m in test.maintainers %}
                        # {{ m.name | e }} {{ '<%s>' | format(m.email) | e }}
                      {% endfor %}
                    {% endfor %}
                  {% endfor %}
                {% endfor %}
              {% endfor %}
            </job>
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>.*case1.*'
                r'# Some Maintainer &lt;someone@maintainers.org&gt;.*</job>.*')

    def test_tests_argument(self):
        """Test matching with --tests argument"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                    B:
                      name: B
                      location: somewhere
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      cases:
                        b:
                          name: b
                          max_duration_seconds: 600
                        b2:
                          name: b2
                          max_duration_seconds: 600
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            test_name_re_output_re_map = {"B - b": r"HOST\s*B - b",
                                          "B.*": r"HOST\s*B - b\s*B - b2",
                                          "B": ""}
            for test_name_re in test_name_re_output_re_map:
                with self.subTest(test_name_re=test_name_re):
                    self.assertKpetProduces(
                        kpet_run_generate, db_path, "--tests", test_name_re,
                        stdout_matching=fr'.*'
                        fr'{test_name_re_output_re_map[test_name_re]}'
                        fr'\s*</job>.*')

    def test_maintainer_struct(self):
        """Test cases' "maintainers" field should be exposed to templates"""
        # pylint: disable=C0301
        # flake8: noqa: E501
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      maintainers:
                        - name: Some Maintainer
                          email: someone@maintainers.org
                        - name: Other Maintainer
                          email: other@maintainers.org
                          gitlab: other
            """,
            "beaker.xml.j2": """
            <job>
              {% for scene in SCENES %}
                {% for recipeset in scene.recipesets %}
                  {% for HOST in recipeset %}
                    {% for test in HOST.tests %}
                      {{ test.name }}
                      Maintainers:
                      {% for m in test.maintainers %}
                        {{ '%s <%s> %s' | format(m.name, m.email, m.gitlab|d('NO_GL')) | e }}
                      {% endfor %}
                    {% endfor %}
                  {% endfor %}
                {% endfor %}
              {% endfor %}
            </job>
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>.*case1.*'
                r'.*Some Maintainer &lt;someone@maintainers.org&gt; NO_GL.*'
                r'.*Other Maintainer &lt;other@maintainers.org&gt; other.*'
                r'.*</job>.*')

    def test_invalid_maintainer(self):
        """
        Check that Object raiser Invalid exception if the schema doesn't match.
        """
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      maintainers:
                        - name: Some Maintainer
            """,
            "beaker.xml.j2": """
            <job>
              {% for scene in SCENES %}
                {% for recipeset in scene.recipesets %}
                  {% for HOST in recipeset %}
                    {% for test in HOST.tests %}
                      {{ test.name }}
                    {% endfor %}
                  {% endfor %}
                {% endfor %}
              {% endfor %}
            </job>
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_test_list, db_path, status=1,
                stderr_matching=r'.*Invalid: Member "email" is missing.*')

    def test_global_template_present(self):
        """
        Check that global template specification is accepted
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  name: Test
                  host_type_regex: normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "output.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_matching=r'Test')

    def test_global_template_missing(self):
        """
        Check that unspecified global template is accepted
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  name: Test
                  host_type_regex: normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_matching=r'')

    def test_host_type_without_recipeset(self):
        """
        Check a host type without recipeset gets one created and assigned
        """
        assets = {
            "index.yaml": """
                template: tree.txt.j2
                host_types:
                    a: {}
                    b: {}
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  name: Test
                  host_type_regex: b
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "tree.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_matching=r'Test')

    def test_two_host_types_without_recipeset(self):
        """
        Check two host types without recipesets get an implicit one each
        """
        assets = {
            "index.yaml": """
                template: tree.txt.j2
                host_types:
                    a: {}
                    b: {}
                    c: {}
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    test1:
                      name: Test1
                      host_type_regex: b
                    test2:
                      name: Test2
                      host_type_regex: c
            """,
            "tree.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {{- "RECIPESET:" -}}
                {%- for HOST in recipeset -%}
                  {{- "HOST:" -}}
                  {%- for test in HOST.tests -%}
                    {{- test.name + "\\n" -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_matching='RECIPESET:HOST:Test1\n'
                                'RECIPESET:HOST:Test2\n')

    def test_empty_recipesets(self):
        """
        Check run generation works with empty "recipesets"
        """
        assets = {
            "index.yaml": """
                template: tree.txt.j2
                host_types:
                    a: {}
                recipesets: {}
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  name: Test
                  host_type_regex: a
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "tree.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_matching=r'Test')

    def test_no_recipesets(self):
        """
        Check run generation works without "recipesets"
        """
        assets = {
            "index.yaml": """
                template: tree.txt.j2
                host_types:
                    a: {}
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  name: Test
                  host_type_regex: a
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "tree.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_matching=r'Test')

    def test_no_recipesets_with_domains(self):
        """
        Check run generation works without "recipesets" and with domains
        """
        assets = {
            "index.yaml": """
                template: tree.txt.j2
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    a:
                        domains: general
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  name: Test
                  host_type_regex: a
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "tree.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_matching=r'Test')

    def test_empty_recipesets_with_domains(self):
        """
        Check run generation works with empty "recipesets" and with domains
        """
        assets = {
            "index.yaml": """
                template: tree.txt.j2
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    a:
                        domains: general
                recipesets: {}
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  name: Test
                  host_type_regex: a
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "tree.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_matching=r'Test')
