# Copyright (c) 2021 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for host domains"""
from tests.test_integration import (IntegrationTests, assets_mkdir,
                                    kpet_run_generate, kpet_run_test_list,
                                    kpet_test_list)

# It's OK, pylint: disable=too-many-lines

OUTPUT_TXT_J2 = """
    {%- for scene in SCENES -%}
        {{- "SCENE:" -}}
        {%- for recipeset in scene.recipesets -%}
            {{- "RECIPESET:" -}}
            {%- for host in recipeset -%}
                {{- "HOST<" -}}
                {%- set sep = joiner(",") -%}
                {%- if host.hostname -%}
                    {{- sep() + "name:" + host.hostname -}}
                {%- endif -%}
                {%- for not_hostname in host.not_hostnames -%}
                    {{- sep() + "not_name:" + not_hostname -}}
                {%- endfor -%}
                {%- for host_requires in host.host_requires_list -%}
                    {{- sep() + "requires:" + host_requires -}}
                {%- endfor -%}
                {%- for host_rejects in host.host_rejects_list -%}
                    {{- sep() + "rejects:" + host_rejects -}}
                {%- endfor -%}
                {{- ">:" -}}
                {%- for test in host.tests -%}
                    {{- "TEST:" + test.name -}}
                {%- endfor -%}
            {%- endfor -%}
        {%- endfor -%}
    {%- endfor -%}
"""


class IntegrationDomainsTests(IntegrationTests):
    """Integration tests for host domain handling"""

    def test_no_domains_anywhere(self):
        """Check no domains defined, no domains in host types works"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    name: Test
                    host_type_regex: normal
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<>:TEST:Test'
            )

    def test_domains_in_host_types_only(self):
        """Check domains defined only in host types breaks"""
        assets = {
            "index.yaml": """
                host_types:
                    normal:
                        domains: general
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    name: Test
                    host_type_regex: normal
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=1,
                stderr_matching=r".* Host type 'normal' specifies "
                                r'"domains" but domains are not defined.*'
            )

    def test_domain_defined_but_not_used(self):
        """
        Check hosts are not instantiated when a domain is defined,
        but not used in host types.
        """
        assets = {
            "index.yaml": """
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    name: Test
                    host_type_regex: normal
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:'
            )

    def test_two_domains_defined_but_not_used(self):
        """
        Check hosts are not instantiated when two domains are defined,
        but not used in host types.
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    name: Test
                    host_type_regex: normal
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:SCENE:'
            )

    def test_domain_defined_but_partially_used(self):
        """
        Check specific hosts are not instantiated when a domain is defined,
        but not used in corresponding host types.
        """
        assets = {
            "index.yaml": """
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    a:
                        domains: general
                    b: {}
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<>:TEST:Test1'
            )

    def test_domain_everywhere(self):
        """
        Test a domain is defined and used everywhere.
        """
        assets = {
            "index.yaml": """
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    a:
                        domains: general
                    b:
                        domains: general
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<>:TEST:Test1'
                                r'RECIPESET:HOST<>:TEST:Test2'
            )

    def test_two_domains_everywhere(self):
        """
        Test two domains are defined and used everywhere.
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                    b:
                        domains: Y
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<>:TEST:Test1'
                                r'SCENE:RECIPESET:HOST<>:TEST:Test2'
            )

    def test_unknown_domain(self):
        """
        Test host type not matching any known domain is detected
        """
        assets = {
            "index.yaml": """
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    a:
                        domains: special
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_type_regex: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=1,
                stderr_matching=r'.*none of the known domains.*'
            )

    def test_one_domain_with_host_requires(self):
        """
        Test one domain with host_requires is handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    general:
                        description: Generally-available hosts
                        host_requires: A
                host_types:
                    a:
                        domains: general
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_type_regex: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<requires:A>:TEST:Test'
            )

    def test_one_domain_with_host_requires_one_without(self):
        """
        Test one domain with host_requires, and one without, are handled
        correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_type_regex: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<requires:x>:TEST:Test'
                                r'SCENE:'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                    b:
                        domains: Y
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<requires:x>:TEST:Test1'
                                r'SCENE:RECIPESET:HOST<rejects:x>:TEST:Test2'
            )

    def test_two_domains_with_host_requires(self):
        """
        Test two domains, both with host_requires, are handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                        host_requires: y
                host_types:
                    a:
                        domains: X
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_type_regex: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<requires:x,rejects:y>:'
                                r'TEST:Test'
                                r'SCENE:'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                        host_requires: y
                host_types:
                    a:
                        domains: X
                    b:
                        domains: Y
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<requires:x,rejects:y>:'
                                r'TEST:Test1'
                                r'SCENE:RECIPESET:HOST<requires:y,rejects:x>:'
                                r'TEST:Test2'
            )

    def test_one_domain_with_hostname(self):
        """
        Test one domain with a host type using "hostname" property is handled
        correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_type_regex: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<name:a.com>:TEST:Test'
            )

    def test_one_domain_with_hostname_one_without(self):
        """
        Test one domain with a host type using "hostname" property, and one
        without, are handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                    b:
                        domains: Y
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_type_regex: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<name:a.com>:TEST:Test'
                                r'SCENE:'
            )

    def test_two_domains_with_hostnames(self):
        """
        Test two domains, each with a host type using "hostname" property,
        are handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                    b:
                        domains: Y
                        hostname: b.com
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_type_regex: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:'
                                r'HOST<name:a.com,not_name:b.com>:TEST:Test'
                                r'SCENE:'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                    b:
                        domains: Y
                        hostname: b.com
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:'
                                r'HOST<name:a.com,not_name:b.com>:TEST:Test1'
                                r'SCENE:RECIPESET:'
                                r'HOST<name:b.com,not_name:a.com>:TEST:Test2'
            )

    def test_two_domains_with_multiple_hostnames(self):
        """
        Test two domains, each with multiple host types, each using "hostname"
        property, are handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                    b:
                        domains: X
                        hostname: b.com
                    c:
                        domains: Y
                        hostname: c.com
                    d:
                        domains: Y
                        hostname: d.com
                recipesets:
                    rcs1:
                      - a
                      - b
                    rcs2:
                      - c
                      - d
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r''
                r'SCENE:RECIPESET:'
                r'HOST<name:a.com,not_name:c.com,not_name:d.com>:'
                r'TEST:Test1'
                r'HOST<name:b.com,not_name:c.com,not_name:d.com>:'
                r'TEST:Test2'
                r'SCENE:'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                    b:
                        domains: X
                        hostname: b.com
                    c:
                        domains: Y
                        hostname: c.com
                    d:
                        domains: Y
                        hostname: d.com
                recipesets:
                    rcs1:
                      - a
                      - b
                    rcs2:
                      - c
                      - d
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
                        test3:
                            name: Test3
                            host_type_regex: c
                        test4:
                            name: Test4
                            host_type_regex: d
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r''
                r'SCENE:RECIPESET:'
                r'HOST<name:a.com,not_name:c.com,not_name:d.com>:'
                r'TEST:Test1'
                r'HOST<name:b.com,not_name:c.com,not_name:d.com>:'
                r'TEST:Test2'
                r'SCENE:RECIPESET:'
                r'HOST<name:c.com,not_name:a.com,not_name:b.com>:'
                r'TEST:Test3'
                r'HOST<name:d.com,not_name:a.com,not_name:b.com>:'
                r'TEST:Test4'
            )

    def test_host_type_in_two_domains(self):
        """
        Test a host type in two domains is handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                    Z:
                        description: Hosts Z
                host_types:
                    a:
                        domains: X|Y
                    b:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<>:TEST:Test1'
                                r'SCENE:RECIPESET:HOST<>:TEST:Test1'
                                r'SCENE:RECIPESET:HOST<>:TEST:Test2'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                        host_requires: y
                    Z:
                        description: Hosts Z
                        host_requires: z
                host_types:
                    a:
                        domains: X|Y
                    b:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r''
                r'SCENE:RECIPESET:HOST<requires:x,rejects:y,rejects:z>:'
                r'TEST:Test1'
                r'SCENE:RECIPESET:HOST<requires:y,rejects:x,rejects:z>:'
                r'TEST:Test1'
                r'SCENE:RECIPESET:HOST<requires:z,rejects:x,rejects:y>:'
                r'TEST:Test2'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                        host_requires: y
                    Z:
                        description: Hosts Z
                        host_requires: z
                host_types:
                    a:
                        domains: X|Y
                        hostname: a.com
                    b:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r''
                r'SCENE:RECIPESET:'
                r'HOST<name:a.com,requires:x,rejects:y,rejects:z>:'
                r'TEST:Test1'
                r'SCENE:RECIPESET:'
                r'HOST<name:a.com,requires:y,rejects:x,rejects:z>:'
                r'TEST:Test1'
                r'SCENE:RECIPESET:'
                r'HOST<not_name:a.com,requires:z,rejects:x,rejects:y>:'
                r'TEST:Test2'
            )

    def test_recipeset_in_multiple_domains(self):
        """
        Test a recipeset with host types in multiple domains is handled
        correctly.
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                    Z:
                        description: Hosts Z
                host_types:
                    a:
                        domains: X|Y
                    b:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:HOST<>:TEST:Test1'
                                r'SCENE:RECIPESET:HOST<>:TEST:Test1'
                                r'SCENE:RECIPESET:HOST<>:TEST:Test2'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                    Z:
                        description: Hosts Z
                host_types:
                    a:
                        domains: X|Y
                    b:
                        domains: X
                    c:
                        domains: Y
                    d:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                      - b
                      - c
                      - d
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
                        test3:
                            name: Test3
                            host_type_regex: c
                        test4:
                            name: Test4
                            host_type_regex: d
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'SCENE:RECIPESET:'
                                r'HOST<>:TEST:Test1'
                                r'HOST<>:TEST:Test2'
                                r'SCENE:RECIPESET:'
                                r'HOST<>:TEST:Test1'
                                r'HOST<>:TEST:Test3'
                                r'SCENE:RECIPESET:'
                                r'HOST<>:TEST:Test4'
            )

    def test_domain_selection(self):
        """
        Test domain selection from command line works correctly
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    name: Test
                    host_type_regex: normal
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", ".*", status=1,
                stderr_matching=r'.*no domains specified.*'
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--domains", ".*", status=1,
                stderr_matching=r'.*no domains specified.*'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--domains", ".*", status=1,
                stderr_matching=r'.*no domains specified.*'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                    Z:
                        description: Hosts Z
                host_types:
                    a:
                        domains: X|Y
                    b:
                        domains: X
                    c:
                        domains: Y
                    d:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                      - b
                      - c
                      - d
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_type_regex: a
                        test2:
                            name: Test2
                            host_type_regex: b
                        test3:
                            name: Test3
                            host_type_regex: c
                        test4:
                            name: Test4
                            host_type_regex: d
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", ".*",
                stdout_matching=r'SCENE:RECIPESET:'
                                r'HOST<>:TEST:Test1'
                                r'HOST<>:TEST:Test2'
                                r'SCENE:RECIPESET:'
                                r'HOST<>:TEST:Test1'
                                r'HOST<>:TEST:Test3'
                                r'SCENE:RECIPESET:'
                                r'HOST<>:TEST:Test4'
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--domains", ".*",
                stdout_matching=r'Test1\nTest2\nTest3\nTest4\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--domains", ".*",
                stdout_matching=r'Test1\nTest2\nTest3\nTest4\n'
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "X",
                stdout_matching=r'SCENE:RECIPESET:'
                                r'HOST<>:TEST:Test1'
                                r'HOST<>:TEST:Test2'
            )
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "X|Y",
                stdout_matching=r'SCENE:RECIPESET:'
                                r'HOST<>:TEST:Test1'
                                r'HOST<>:TEST:Test2'
                                r'SCENE:RECIPESET:'
                                r'HOST<>:TEST:Test1'
                                r'HOST<>:TEST:Test3'
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--domains", "X|Y",
                stdout_matching=r'Test1\nTest2\nTest3\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--domains", "X|Y",
                stdout_matching=r'Test1\nTest2\nTest3\n'
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "Z",
                stdout_matching=r'SCENE:RECIPESET:'
                                r'HOST<>:TEST:Test4'
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--domains", "Z",
                stdout_matching=r'Test4\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--domains", "Z",
                stdout_matching=r'Test4\n'
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "A", status=1,
                stderr_matching=r'.*matches no domains.*'
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--domains", "A", status=1,
                stderr_matching=r'.*matches no domains.*'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--domains", "A", status=1,
                stderr_matching=r'.*matches no domains.*'
            )

    def test_architecture_filtering(self):
        """
        Check domains and host types are properly architecture-filtered
        """
        assets = {
            "index.yaml": """
                domains:
                    no_arch:
                        description: Domain without architecture
                    arch_X:
                        description: Domain with architecture X
                        arches: X
                    arch_Y:
                        description: Domain with architecture Y
                        arches: Y
                host_types:
                    all_domains_no_arches:
                        domains: .*
                    all_domains_arch_X:
                        domains: .*
                        arches: X
                    all_domains_arch_Y:
                        domains: .*
                        arches: Y
                    all_domains_all_arches:
                        domains: .*
                        arches: .*

                    no_arch_domain_no_arches:
                        domains: no_arch
                    no_arch_domain_arch_X:
                        domains: no_arch
                        arches: X
                    no_arch_domain_arch_Y:
                        domains: no_arch
                        arches: Y
                    no_arch_domain_all_arches:
                        domains: no_arch
                        arches: .*

                    arch_X_domain_no_arches:
                        domains: arch_X
                    arch_X_domain_arch_X:
                        domains: arch_X
                        arches: X
                    arch_X_domain_arch_Y:
                        domains: arch_X
                        arches: Y
                    arch_X_domain_all_arches:
                        domains: arch_X
                        arches: .*

                    arch_Y_domain_no_arches:
                        domains: arch_Y
                    arch_Y_domain_arch_X:
                        domains: arch_Y
                        arches: X
                    arch_Y_domain_arch_Y:
                        domains: arch_Y
                        arches: Y
                    arch_Y_domain_all_arches:
                        domains: arch_Y
                        arches: .*
                arches:
                    - X
                    - Y
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        all_domains_no_arches:
                            name: all_domains_no_arches
                            host_type_regex: all_domains_no_arches
                        all_domains_arch_X:
                            name: all_domains_arch_X
                            host_type_regex: all_domains_arch_X
                        all_domains_arch_Y:
                            name: all_domains_arch_Y
                            host_type_regex: all_domains_arch_Y
                        all_domains_all_arches:
                            name: all_domains_all_arches
                            host_type_regex: all_domains_all_arches

                        no_arch_domain_no_arches:
                            name: no_arch_domain_no_arches
                            host_type_regex: no_arch_domain_no_arches
                        no_arch_domain_arch_X:
                            name: no_arch_domain_arch_X
                            host_type_regex: no_arch_domain_arch_X
                        no_arch_domain_arch_Y:
                            name: no_arch_domain_arch_Y
                            host_type_regex: no_arch_domain_arch_Y
                        no_arch_domain_all_arches:
                            name: no_arch_domain_all_arches
                            host_type_regex: no_arch_domain_all_arches

                        arch_X_domain_no_arches:
                            name: arch_X_domain_no_arches
                            host_type_regex: arch_X_domain_no_arches
                        arch_X_domain_arch_X:
                            name: arch_X_domain_arch_X
                            host_type_regex: arch_X_domain_arch_X
                        arch_X_domain_arch_Y:
                            name: arch_X_domain_arch_Y
                            host_type_regex: arch_X_domain_arch_Y
                        arch_X_domain_all_arches:
                            name: arch_X_domain_all_arches
                            host_type_regex: arch_X_domain_all_arches

                        arch_Y_domain_no_arches:
                            name: arch_Y_domain_no_arches
                            host_type_regex: arch_Y_domain_no_arches
                        arch_Y_domain_arch_X:
                            name: arch_Y_domain_arch_X
                            host_type_regex: arch_Y_domain_arch_X
                        arch_Y_domain_arch_Y:
                            name: arch_Y_domain_arch_Y
                            host_type_regex: arch_Y_domain_arch_Y
                        arch_Y_domain_all_arches:
                            name: arch_Y_domain_all_arches
                            host_type_regex: arch_Y_domain_all_arches
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        domains_arches_tests = {
            ".*": {
                None: """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_arch_Y
                    arch_X_domain_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_X
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
                ".*": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
                "X": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_no_arches
                """.split(),
                "Y": """
                    all_domains_all_arches
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
            },
            "no_arch": {
                None: """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
                ".*": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
                "X": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_no_arches
                """.split(),
                "Y": """
                    all_domains_all_arches
                    all_domains_arch_Y
                    all_domains_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
            },
            "arch_X": {
                None: """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_arch_Y
                    arch_X_domain_no_arches
                """.split(),
                ".*": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_no_arches
                """.split(),
                "X": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_no_arches
                """.split(),
                "Y": "",
            },
            "arch_Y": {
                None: """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_X
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                """.split(),
                ".*": """
                    all_domains_all_arches
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                """.split(),
                "X": "",
                "Y": """
                    all_domains_all_arches
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                """.split(),
            },
        }
        with assets_mkdir(assets) as db_path:
            for domains, arches_tests in domains_arches_tests.items():
                for arch, tests in arches_tests.items():
                    args = [kpet_test_list, db_path, "--domains", domains]
                    if arch is not None:
                        args += ["--arch", arch]
                    self.assertKpetProduces(
                        *args,
                        stdout_matching="".join(
                            f'{test}\n'
                            for test in tests
                        )
                    )
