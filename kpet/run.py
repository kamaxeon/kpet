# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Execution of tests from the database"""

import re
import jinja2
from lxml import etree
from kpet import data


class Test:     # pylint: disable=too-few-public-methods
    """
    A test run.
    """
    # make kpet.data.Test attributes accessible without having to use `.data`
    def __getattr__(self, attr):
        return getattr(self.data, attr)

    def __init__(self, test_data, waived):
        """
        Initialize a test run.

        Args:
            test_data:  The executed test's data (a kpet.data.Test instance).
            waived:     True if the test result result is waived (should be
                        omitted from the summary). False otherwise.
        """
        assert isinstance(test_data, data.Test)
        assert isinstance(waived, bool)
        self.data = test_data
        self.waived = waived


class Host:
    # pylint: disable=too-few-public-methods, too-many-instance-attributes
    """A host running tests"""

    def __init__(self, type_name, type, not_hostnames,
                 host_requires_list, host_rejects_list, tests):
        """
        Initialize a host run.

        Args:
            type_name:          Name of the host type.
            type:               Type of the host.
            not_hostnames:      A list of FQDNs of machines which shouldn't be
                                picked for this host.
            host_requires_list: A list of strings expressing extra
                                requirements for this host.
            host_rejects_list:  A list of strings expressing extra
                                negative requirements for this host.
            tests:              A list of tests (kpet.run.Test instances).
        """
        # pylint: disable=too-many-arguments, too-many-locals
        assert isinstance(type_name, str)
        assert isinstance(type, data.HostType)
        assert isinstance(not_hostnames, list)
        assert all(isinstance(s, str) for s in not_hostnames)
        assert isinstance(host_requires_list, list)
        assert all(isinstance(s, str) for s in host_requires_list)
        assert isinstance(host_rejects_list, list)
        assert all(isinstance(s, str) for s in host_rejects_list)
        assert isinstance(tests, list)
        assert all(isinstance(test, Test) for test in tests)

        self.type_name = type_name
        self.hostname = type.hostname
        self.not_hostnames = not_hostnames.copy()
        self.ignore_panic = type.ignore_panic
        self.preboot_tasks = type.preboot_tasks
        self.postboot_tasks = type.postboot_tasks
        self.tests = tests

        # Collect all unique cases referenced by tests,
        # in per-test top-bottom series
        cases = []
        for test in tests:
            pos = len(cases)
            case = test.case
            while case is not None:
                if case not in cases:
                    cases.insert(pos, case)
                case = case.parent

        # Assemble host_requires, host_rejects, partitions and kickstart lists
        host_requires_list = host_requires_list.copy()
        host_rejects_list = host_rejects_list.copy()
        host_requires_list.append(type.host_requires)
        partitions_list = [type.partitions]
        kickstart_list = [type.kickstart]
        for case in cases:
            host_requires_list.append(case.host_requires)
            partitions_list.append(case.partitions)
            kickstart_list.append(case.kickstart)

        # Remove undefined template paths
        self.host_requires_list = filter(lambda e: e is not None,
                                         host_requires_list)
        self.host_rejects_list = filter(lambda e: e is not None,
                                        host_rejects_list)
        self.partitions_list = filter(lambda e: e is not None,
                                      partitions_list)
        self.kickstart_list = filter(lambda e: e is not None,
                                     kickstart_list)

        # Put waived tests at the end
        self.tests.sort(key=lambda t: t.waived)


class Scene:        # pylint: disable=too-few-public-methods
    """A single run of tests in a database"""
    # pylint: disable=too-many-arguments, too-many-locals, too-many-branches
    def __init__(self, database, domain, target,
                 match_sets=None, test_regexes=None, kernel=None):
        """
        Initialize a test execution scene.

        Args:
            database:       The database to get test data from.
            domain:         The name of the host domain to run tests on.
                            Must be present in the database. No effect, if the
                            database has no domains defined.
            target:         A test execution target (a kpet.data.Target
                            instance) to match/filter tests with.
                            Each target's tree, architecture, and component
                            must be present in the database. Both the target's
                            tree and architecture must be known (not
                            kpet.data.Target.UNKNOWN), for the scene (its
                            tests) to be executable.
            match_sets:     A function matching sets a test belongs to and
                            returning True, if it should be included in the
                            filtering, and False, if not. Must accept a set of
                            strings (names of sets the test belongs to) as the
                            only argument. Can be None to match any set
                            membership. Default is None.
            test_regexes:   A list of string regexes to match
                            test names against.
                            Tests that match any regex will be included in the
                            filtering. None if all tests should be included.
                            Default is None.
            kernel:         An (arbitrary) string representing the kernel
                            location, or None, meaning no kernel was
                            specified. The kernel must be specified for the
                            scene (its tests) to be executable. Default is
                            None.
        """
        assert isinstance(database, data.Base)
        assert database.domains is None or \
            (isinstance(domain, str) and domain in database.domains)
        assert isinstance(target, data.Target)
        assert target.trees is data.Target.UNKNOWN or \
            isinstance(target.trees, set) and \
            target.trees <= set(database.trees)
        assert target.arches is data.Target.UNKNOWN or \
            isinstance(target.arches, set) and \
            target.arches <= set(database.arches)
        assert target.components is data.Target.UNKNOWN or \
            target.components is data.Target.ALL or \
            target.components <= set(database.components)
        assert match_sets is None or callable(match_sets)
        assert test_regexes is None or \
            (isinstance(test_regexes, list) and
             all(isinstance(regex, str) for regex in test_regexes))
        assert kernel is None or isinstance(kernel, str)

        match_sets = match_sets or (lambda _: True)

        self.kernel = kernel
        self.arch = None if target.arches is data.Target.UNKNOWN \
            else tuple(target.arches)[0]
        self.tree = None if target.trees is data.Target.UNKNOWN \
            else tuple(target.trees)[0]

        # Create test runs for tests matching the parameters
        tests = []
        for test_data in database.tests.values():
            if (test_regexes is None or
                any(re.fullmatch(regex, test_data.name)
                    for regex in test_regexes)) and \
               match_sets(test_data.sets) and \
               test_data.is_enabled_for(target):
                tests.append(Test(test_data,
                                  test_data.is_waived_for(target)))

        # Collect host_requires_list and host_rejects_list for this domain
        host_requires_list = []
        host_rejects_list = []
        for other_name, other_data in (database.domains or {}).items():
            if other_data.host_requires is not None:
                (host_requires_list if other_name == domain
                 else host_rejects_list).append(other_data.host_requires)

        # Collect this domain's matching host types
        # and hostnames exclusive to other domains
        domain_arches = database.arches if database.domains is None \
            else database.domains[domain].arches
        host_types = {}
        not_hostnames = set()
        for host_type_name, host_type_data in database.host_types.items():
            if database.domains is None or domain in host_type_data.domains:
                if self.arch is None or \
                       (self.arch in host_type_data.arches and
                        self.arch in domain_arches):
                    host_types[host_type_name] = host_type_data
            elif host_type_data.hostname is not None:
                not_hostnames.add(host_type_data.hostname)
        not_hostnames = list(sorted(not_hostnames))

        # Distribute tests to available host types
        host_types_tests = {}
        for test in tests:
            # Assign the test to a host type
            for host_type_name in host_types:
                if not test.host_type_regex or \
                   test.host_type_regex.fullmatch(host_type_name):
                    host_type_tests = host_types_tests.get(host_type_name, [])
                    host_type_tests.append(test)
                    host_types_tests[host_type_name] = host_type_tests
                    break

        # Distribute host types to recipesets
        self.recipesets = []
        # For each list of host type names in every recipeset def
        for recipeset_host_type_names in database.recipesets.values():
            # Collect hosts and tests for the recipeset
            recipeset = []
            # For each host type name in the recipeset def
            for recipeset_host_type_name in recipeset_host_type_names:
                # If the host type is not available or has no tests
                if recipeset_host_type_name not in host_types_tests:
                    continue
                # Consume the host type and its tests
                recipeset.append(Host(
                    type_name=recipeset_host_type_name,
                    type=host_types[recipeset_host_type_name],
                    not_hostnames=not_hostnames,
                    host_requires_list=host_requires_list,
                    host_rejects_list=host_rejects_list,
                    tests=host_types_tests.pop(recipeset_host_type_name)
                ))
            if recipeset:
                self.recipesets.append(recipeset)

        # Put hosts for remaining types into dedicated implicit recipesets
        for host_type_name, host_type_tests in host_types_tests.items():
            self.recipesets.append([Host(
                type_name=host_type_name,
                type=host_types[host_type_name],
                not_hostnames=not_hostnames,
                host_requires_list=host_requires_list,
                host_rejects_list=host_rejects_list,
                tests=host_type_tests
            )])

    def is_executable(self):
        """
        Check if the scene is executable (has all the necessary data for its
        tests to run).

        Returns:
            True if the scene is executable. False otherwise.
        """
        return not (
            self.kernel is None or self.arch is None or self.tree is None
        )

    def get_tests(self):
        """
        Get a set of data objects (kpet.data.Test instances) of tests executed
        in the scene.

        Returns:
            The set of data objects for executed tests.
        """
        return {
            test.data
            for recipeset in self.recipesets
            for host in recipeset
            for test in host.tests
        }


class Scenario:     # pylint: disable=too-few-public-methods
    """A collection of scenes (runs of tests in a database)"""
    def __init__(self, database):
        """
        Initialize a test execution scenario.

        Args:
            database:   The database to use as the data source for test
                        execution scenes, and for generating the run XML.
        """
        assert isinstance(database, data.Base)
        self.database = database
        self.scenes = []

    # pylint: disable=too-many-arguments
    def add_scene(self, domain, target,
                  match_sets=None, test_regexes=None, kernel=None):
        """
        Add a new scene to the scenario using the database the scenario was
        created with and the specified scene parameters.

        Args:
            domain:         The name of the host domain to run tests on.
                            Must be present in the database. No effect, if the
                            database has no domains defined.
            target:         A test execution target (a kpet.data.Target
                            instance) to match/filter tests with.
                            Each target's tree, architecture, and component
                            must be present in the database. Both the target's
                            tree and architecture must be known (not
                            kpet.data.Target.UNKNOWN), for the scene (its
                            tests) to be executable.
            match_sets:     A function matching sets a test belongs to and
                            returning True, if it should be included in the
                            filtering, and False, if not. Must accept a set of
                            strings (names of sets the test belongs to) as the
                            only argument. Can be None to match any set
                            membership. Default is None.
            test_regexes:   A list of string regexes to match
                            test names against.
                            Tests that match any regex will be included in the
                            filtering. None if all tests should be included.
                            Default is None.
            kernel:         An (arbitrary) string representing the kernel
                            location, or None, meaning no kernel was
                            specified. The kernel must be specified for the
                            scene (its tests) to be executable. Default is
                            None.
        """
        assert self.database.domains is None or \
            (isinstance(domain, str) and domain in self.database.domains)
        assert isinstance(target, data.Target)
        assert target.trees is data.Target.UNKNOWN or \
            isinstance(target.trees, set) and \
            target.trees <= set(self.database.trees)
        assert target.arches is data.Target.UNKNOWN or \
            isinstance(target.arches, set) and \
            target.arches <= set(self.database.arches)
        assert target.components is data.Target.UNKNOWN or \
            target.components is data.Target.ALL or \
            target.components <= set(self.database.components)
        assert match_sets is None or callable(match_sets)
        assert test_regexes is None or \
            (isinstance(test_regexes, list) and
             all(isinstance(regex, str) for regex in test_regexes))
        assert kernel is None or isinstance(kernel, str)
        self.scenes.append(Scene(
            database=self.database,
            domain=domain,
            target=target,
            match_sets=match_sets,
            test_regexes=test_regexes,
            kernel=kernel
        ))

    def is_executable(self):
        """
        Check if the scenario is executable (all its scenes are executable,
        i.e. have enough data for tests to run).

        Returns:
            True if the scenario is executable. False otherwise.
        """
        return all(scene.is_executable() for scene in self.scenes)

    def generate(self, description, lint, variables):
        """
        Generate an XML document describing the hosts and tests involved in
        executing the scenario. The scenario must be executable for this to
        succeed.

        Args:
            description:    The run description string.
            lint:           Lint and reformat the resulting XML, if True.
            variables:      A dictionary of extra template variables.
        Returns:
            A string containing the generated XML document.
        """
        assert isinstance(description, str)
        assert isinstance(lint, bool)
        assert isinstance(variables, dict)
        assert self.is_executable()

        template_path = self.database.template
        if template_path is None:
            return ""

        params = dict(
            DESCRIPTION=description,
            SCENES=self.scenes,
            VARIABLES=variables,
        )

        jinja_env = jinja2.Environment(
            loader=jinja2.FileSystemLoader([self.database.dir_path]),
            trim_blocks=True,
            keep_trailing_newline=True,
            lstrip_blocks=True,
            autoescape=jinja2.select_autoescape(
                enabled_extensions=('xml'),
                default_for_string=True,
            ),
            undefined=jinja2.StrictUndefined,
        )
        template = jinja_env.get_template(template_path)
        text = template.render(params)

        if lint:
            parser = etree.XMLParser(remove_blank_text=True)
            tree = etree.XML(text.encode("utf-8"), parser)
            text = etree.tostring(tree, encoding="utf-8",
                                  xml_declaration=True,
                                  pretty_print=True).decode("utf-8")
        return text

    def get_tests(self):
        """
        Get a set of data objects (kpet.data.Test instances) of tests executed
        in the scenario.

        Returns:
            The set of data objects for executed tests.
        """
        tests = set()
        for scene in self.scenes:
            tests.update(scene.get_tests())
        return tests
