# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""The "run" command"""
import http.cookiejar as cookiejar
import re
import sys
from kpet import misc, patch, data, run, ssp, cmd_misc


def build_common(parser):
    """
    Add arguments to a "run" sub-command parser, which are common between
    sub-commands.

    Args:
        parser: The parser to add arguments to.
    """
    parser.add_argument(
        '--cookies',
        metavar='FILE',
        default=None,
        help='Cookies to send when downloading patches, Netscape-format file.'
    )
    parser.add_argument(
        '--domains',
        metavar='REGEX',
        help='A regular expression matching domains to restrict the run to. '
        'Only hosts belonging to the matching domains will be available '
        'to tests. Not allowed, if the database has no domains defined. ' +
        'Run "kpet domain list" to see all available domains.'
    )
    parser.add_argument(
        '-t',
        '--tree',
        required=True,
        help='Name of the specified kernel\'s tree. ' +
        'Run "kpet tree list" to see recognized trees.'
    )
    parser.add_argument(
        '-a',
        '--arch',
        required=True,
        help='Architecture of the specified kernel. ' +
        'Run "kpet arch list" to see supported architectures.'
    )
    parser.add_argument(
        '-c',
        '--components',
        metavar='REGEX',
        help='A regular expression matching extra components included ' +
        'into the kernel build. ' +
        'Run "kpet component list" to see recognized components.'
    )
    parser.add_argument(
        '-s',
        '--sets',
        metavar='PATTERN',
        help='Test set pattern: regexes (fully) matching names of test sets '
        'to restrict the run to, combined using &, |, !, and () operators, '
        'which can be escaped with \\. Run "kpet set list" to see available '
        'sets.'
    )
    parser.add_argument(
        '--tests',
        action='append',
        metavar='REGEX',
        help='A regular expression that test names must match. '
             'Run "kpet test list" to see available tests.'
    )
    parser.add_argument(
        'mboxes',
        metavar='MBOX',
        nargs='*',
        default=[],
        help='URL/path of a mailbox containing tested patches'
    )


def build(cmds_parser, common_parser):
    """Build the argument parser for the run command"""
    _, action_subparser = cmd_misc.build(
        cmds_parser,
        common_parser,
        'run',
        help='Test suite run',
    )
    generate_parser = action_subparser.add_parser(
        "generate",
        help='Generate the information required for a test run',
        parents=[common_parser],
    )
    generate_parser.add_argument(
        '-d',
        '--description',
        default='',
        help='An arbitrary text describing the run'
    )
    generate_parser.add_argument(
        '-o',
        '--output',
        metavar='FILE',
        default=None,
        help='File to write the output to, default is stdout'
    )
    generate_parser.add_argument(
        '-k',
        '--kernel',
        required=True,
        help='Kernel location. Must be accessible by Beaker.'
    )
    generate_parser.add_argument(
        '--no-lint',
        action='store_true',
        help='Do not lint or reformat output XML'
    )
    generate_parser.add_argument(
        '-v',
        '--variable',
        metavar='NAME=VALUE',
        action='append',
        dest='variables',
        default=[],
        help='Assign a value to a template variable. '
             'Run "kpet variable list" to see recognized variables.'
    )
    build_common(generate_parser)

    test_parser = action_subparser.add_parser(
        "test",
        help="",
        parents=[common_parser],
    )
    test_subaction_subparser = test_parser.add_subparsers(
        title="test_subaction",
        dest="test_subaction",
    )
    test_list_parser = test_subaction_subparser.add_parser(
        "list",
        help='List tests excecuted by run',
        parents=[common_parser],
    )
    build_common(test_list_parser)


# pylint: disable=too-many-branches
def main_create_scenario(args, database):
    """
    Create a scenario for specified test database and command-line arguments.

    Args:
        args:       Parsed command-line arguments.
        database:   The database to create a scenario for.

    Returns:
        The created scenario.
    """
    target_trees = data.Target.UNKNOWN
    target_arches = data.Target.UNKNOWN
    target_components = data.Target.NONE
    target_sources = data.Target.ALL
    match_sets = None

    cookies = cookiejar.MozillaCookieJar()
    if args.cookies:
        cookies.load(args.cookies)
    if args.mboxes:
        target_sources = patch.get_src_set_from_location_set(set(args.mboxes),
                                                             cookies)

    if database.domains is None:
        if args.domains is not None:
            raise Exception("Database has no domains specified, "
                            "but the --domains option is provided")
        domains = [None]
    else:
        domains = list(database.domains)
        if args.domains is not None:
            domain_regex = re.compile(args.domains)
            domains = list(filter(domain_regex.fullmatch, domains))
            if not domains:
                raise Exception(f"Regular expression {args.domains!r} "
                                f"matches no domains")

    if args.tree not in database.trees:
        raise Exception("Tree \"{}\" not found".format(args.tree))
    target_trees = {args.tree}

    if args.arch not in database.arches:
        raise Exception("Architecture \"{}\" not found".format(args.arch))
    if args.arch not in database.trees[args.tree]['arches']:
        raise Exception("Arch \"{}\" not supported by tree \"{}\"".format(
            args.arch, args.tree))
    target_arches = {args.arch}

    if args.components is not None:
        target_components = set(x for x in database.components
                                if re.fullmatch(args.components, x))
    if args.sets is not None:
        try:
            match_sets = ssp.parse(args.sets, set(database.sets))
        except (ssp.InvalidCharacter,
                ssp.InvalidSyntax,
                ssp.VoidRegex) as exc:
            raise Exception(
                f"Failed parsing set pattern: {repr(args.sets)}"
            ) from exc

    target = data.Target(trees=target_trees,
                         arches=target_arches,
                         components=target_components,
                         sources=target_sources)
    scenario = run.Scenario(database)
    for domain in domains:
        scenario.add_scene(domain=domain, target=target,
                           match_sets=match_sets,
                           test_regexes=args.tests,
                           kernel=getattr(args, "kernel", None))
    return scenario


def main_generate(args, scenario):
    """
    Execute `run generate`

    Args:
        args:       Parsed command-line arguments.
        scenario:   The scenario to generate a run from.
    """
    database_variables = scenario.database.variables

    # Parse variable assignments
    variables = {n: i['default']
                 for n, i in database_variables.items()
                 if 'default' in i}
    for assignment in args.variables:
        match = re.fullmatch("([^=]+)=(.*)", assignment)
        if match is None:
            raise Exception(f"Invalid variable assignment: \"{assignment}\"")
        variables[match.group(1)] = match.group(2)

    # Check if all variables are known
    unknown_variables = \
        ", ".join(set(variables.keys()) - set(database_variables.keys()))
    if unknown_variables:
        raise Exception(f"Unknown variables specified: {unknown_variables}.\n"
                        "Run \"kpet variable list\" "
                        "to see recognized variables.")

    # Check if all variables without defaults are set
    unset_variables = \
        ", ".join(
            set(filter(lambda k: "default" not in database_variables[k],
                       database_variables.keys())) -
            set(variables.keys())
        )
    if unset_variables:
        raise Exception(f"Required variables not set: {unset_variables}.\n"
                        "Run \"kpet variable list\" "
                        "to see variable descriptions.\n"
                        "Use -v/--variable NAME=VALUE option "
                        "to specify variable values.")

    content = scenario.generate(description=args.description,
                                lint=not args.no_lint,
                                variables=variables)
    if not args.output:
        sys.stdout.write(content)
    else:
        with open(args.output, 'w') as file_handler:
            file_handler.write(content)


# pylint: disable=unused-argument
def main_test_list(args, scenario):
    """
    Execute `run test list`

    Args:
        args:       Parsed command-line arguments.
        scenario:   The scenario to list the tests of.
    """
    for test_name in sorted(test.name for test in scenario.get_tests()):
        print(test_name)


def main(args):
    """Main function for the `run` command"""
    if not data.Base.is_dir_valid(args.db):
        misc.raise_invalid_database(args.db)
    database = data.Base(args.db)

    if args.action == 'generate':
        main_generate(args, main_create_scenario(args, database))
    elif args.action == 'test':
        if args.test_subaction == 'list':
            main_test_list(args, main_create_scenario(args, database))
        else:
            misc.raise_action_not_found(args.test_subaction, args.command)
    else:
        misc.raise_action_not_found(args.action, args.command)
